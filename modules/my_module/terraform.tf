# TerraForm Backend
terraform {
  backend "s3" {
    bucket         = "tfstate2tonyadolph"
    dynamodb_table = "app-state"
    key    = "path.module/gitlab-bahrain.pem"
    region         = "me-south-1"
  }
}

# AWS Provider
provider "aws" {
  region = "me-south-1"
}

## Import State "global" From Remote S3 Bucket 
#data "terraform_remote_state" "global" {
#  backend = "s3"
#  config = {
#    region = "me-south-1"
#    bucket = "tfstate2tonyadolph"
#    key    = "path.module/gitlab-bahrain.pem"
#  }
#}

resource "aws_security_group" "frontEnd" {
  name        = "frontEnd"
  #description = "Allow TLS inbound traffic"
  #vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["91.74.21.207/32"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    #prefix_list_ids = ["pl-12c4e678"]
  }
}



resource "aws_instance" "default" {
  #user_data = "${data.terraform_remote_state.global.user_data}" 
  user_data = "file(user_data.sh)"
  tags = {
    Name = "deleteMe2"
  }
  ami           = "ami-0c55b36523aa89a7c"
  instance_type = "t3.micro"
  security_groups = [ "frontEnd" ]

  associate_public_ip_address = true
  #key_name                    = data.terraform_remote_state.global.outputs.ssh_pubkey
  key_name                    = "gitlab-bahrain"

  # ignore user_data updates, as this will require a new resource!
  lifecycle {
    ignore_changes = [user_data]
  }
}

#
#resource "null_resource" "default_provisioner" {
#  triggers {
#    default_instance_id = "${aws_instance.default.id}"
#  }
#  
#  connection {
#    host = "${aws_instance.default.public_ip}"
#    type = "ssh"
#    user = "terraform"   # as created in 'user_data'
#    private_key = "${file("/root/.ssh/id_rsa_terraform")}"
#  }
#  # wait for the instance to become available
#  provisioner "remote-exec" {
#    inline = [
#      "echo 'ready'"
#    ]
#  }
#  # ansible provisioner
#  provisioner "ansible" {
#    plays {
#      playbook = {
#        file_path = "${path.module}/ansible/playbook/main.yml"
#        roles_path = [
#          "${path.module}/../../../../../ansible-provisioning/roles",
#        ]
#      }
#    hosts = ["${aws_instance.default.public_ip}"]
#    become = true
#    become_method = "sudo"
#    become_user = "root"
#    
#    extra_vars = {
#      ...
#      ansible_become_pass = "${file("/etc/ansible/become_pass")}"
#    }
#    
#    vault_password_file = "/etc/ansible/vault_password_file"
#  }  
#}
